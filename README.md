# No Más Referidos #

Este script es para eliminar los enlaces referidos que se insertan automáticamente en Forocoches.

Algunos de los enlaces que llevan referidos automáticamente:

- www.amazon.es
- www.ebay.es
- www.aliexpress.com
- www.dealextreme.com
- www.dx.com
- www.focalprice.com
- www.iherb.com

### ¿Qué necesitas? ###

Extensión para el navegador:

* Firefox: [GreaseMonkey](https://addons.mozilla.org/es/firefox/addon/greasemonkey/), [Scriptish](https://addons.mozilla.org/es/firefox/addon/scriptish/)
* Chrome: [TamperMonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=es)

Instalar el script: [referidos-forocoches-jquery.user.js](https://bitbucket.org/goagmx/referidos-forocoches/raw/a887a3bb65ad6baa5dec61d8f2c1abdb2d2d70f5/referidos-forocoches-jquery.user.js)

### Ayuda ###

Si tienes algún problema, puedes dirigirte a: https://bitbucket.org/goagmx/referidos-forocoches/issues?status=new&status=open